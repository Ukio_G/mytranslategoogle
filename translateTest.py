#!/usr/bin/python

import os
import subprocess

def sendmessage(message):
    subprocess.Popen(['notify-send', message])
    return




#from zenipy import message
#subprocess.Popen(['xterm', 'export GOOGLE_APPLICATION_CREDENTIALS=/home/ukio/.myTranslateGoogle/TranslateSelectedTextDebian-fc3e46ab94e2.json'])
from google.cloud import translate

dirname = os.path.dirname(__file__)
fullname = os.path.join(dirname,'key.json')
translate_client = translate.Client.from_service_account_json(fullname)

text = os.popen('xsel').read()
#sendmessage(text)

target = 'ru'

translation = translate_client.translate(
            text,
            target_language=target)

sendmessage(translation['translatedText'])
